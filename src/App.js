import React from "react";

import {
  Row,
  Col,
  Menu,
  Dropdown,
  Alert,
  Button,
  Input,
  Select,
  Card,
} from "antd";

import { DownOutlined } from "@ant-design/icons";

import "./App.css";
// import "bootstrap/dist/css/bootstrap.css"

import fabLogo from "./img/FabLogo.png";
import Highlight1 from "./img/Highlight.png";

const { Search } = Input;
const { Option } = Select;

const selectBefore = (
  <Select defaultValue="Select item" className="select-before">
    <Option value="Professional Services">Professional Services</Option>
    <Option value="Engineering Products">Engineering Products</Option>
    <Option value="CAD Models">CAD Models</Option>
  </Select>
);

const menuKnowledgeBase = (
  <Menu>
    <Menu.Item>
      <a
        target="_blank"
        rel="noopener noreferrer"
        href="http://www.alipay.com/"
      >
        3D Printing
      </a>
    </Menu.Item>
    <Menu.Item>
      <a
        target="_blank"
        rel="noopener noreferrer"
        href="http://www.taobao.com/"
      >
        CNC Manufacturing
      </a>
    </Menu.Item>
  </Menu>
);

const menuProductService = (
  <Menu>
    <Menu.Item>
      <a
        target="_blank"
        rel="noopener noreferrer"
        href="http://www.alipay.com/"
      >
        Products
      </a>
    </Menu.Item>
    <Menu.Item>
      <a
        target="_blank"
        rel="noopener noreferrer"
        href="http://www.taobao.com/"
      >
        Services
      </a>
    </Menu.Item>
  </Menu>
);

const menuIndustry = (
  <Menu>
    <Menu.Item>
      <a
        target="_blank"
        rel="noopener noreferrer"
        href="http://www.alipay.com/"
      >
        Metal Industry
      </a>
    </Menu.Item>
    <Menu.Item>
      <a
        target="_blank"
        rel="noopener noreferrer"
        href="http://www.taobao.com/"
      >
        Rubber Industry
      </a>
    </Menu.Item>
    <Menu.Item>
      <a
        target="_blank"
        rel="noopener noreferrer"
        href="http://www.taobao.com/"
      >
        Plastic Industry
      </a>
    </Menu.Item>
  </Menu>
);

const menuMarketPlace = (
  <Menu>
    <Menu.Item>
      <a
        target="_blank"
        rel="noopener noreferrer"
        href="http://www.alipay.com/"
      >
        Mechanical Items
      </a>
    </Menu.Item>
    <Menu.Item>
      <a
        target="_blank"
        rel="noopener noreferrer"
        href="http://www.taobao.com/"
      >
        Electronic Items
      </a>
    </Menu.Item>
  </Menu>
);

const menuEducationInnovation = (
  <Menu>
    <Menu.Item>
      <a
        target="_blank"
        rel="noopener noreferrer"
        href="http://www.alipay.com/"
      >
        Seminars
      </a>
    </Menu.Item>
    <Menu.Item>
      <a
        target="_blank"
        rel="noopener noreferrer"
        href="http://www.taobao.com/"
      >
        Workshops
      </a>
    </Menu.Item>
  </Menu>
);

function App() {
  return (
    <React.Fragment>
      <div className="Header-Message">
        <Row>
          <Col span="24" offset="0" align="center">
            <Alert
              message="Situational Special Messages and Links"
              type="info"
              closable
            />
          </Col>
        </Row>
      </div>

      <div className="Header-Menu">
        <Row>
          <Col span="3" offset="0" align="center">
            <Dropdown overlay={menuProductService}>
              <a
                className="ant-dropdown-link"
                onClick={(e) => e.preventDefault()}
              >
                Product & Service Intergration
                <DownOutlined />
              </a>
            </Dropdown>
          </Col>

          <Col span="2" offset="0" align="center">
            <Dropdown overlay={menuIndustry}>
              <a
                className="ant-dropdown-link"
                onClick={(e) => e.preventDefault()}
              >
                For Industry
                <DownOutlined />
              </a>
            </Dropdown>
          </Col>

          <Col span="2" offset="0" align="center">
            <Dropdown overlay={menuMarketPlace}>
              <a
                className="ant-dropdown-link"
                onClick={(e) => e.preventDefault()}
              >
                Market Place
                <DownOutlined />
              </a>
            </Dropdown>
          </Col>

          <Col span="3" offset="0" align="center">
            <Dropdown overlay={menuEducationInnovation}>
              <a
                className="ant-dropdown-link"
                onClick={(e) => e.preventDefault()}
              >
                Education / Inovation
                <DownOutlined />
              </a>
            </Dropdown>
          </Col>

          <Col span="8" offset="6" align="right">
            <Button type="link">About</Button>
            <Button type="link">Login</Button>
            <Button type="link">Register</Button>
          </Col>
        </Row>
      </div>

      <div className="Content-logo">
        <Row>
          <Col span="24" offset="0" align="center">
            <img src={fabLogo} alt="FabLanka Logo" width="40%"></img>;
          </Col>
        </Row>
      </div>

      <br />
      <br />

      <div className="Content-InfoText">
        <Row>
          <Col span="24" offset="0" align="center">
            <h1>Bring Your Ideas and Projects to Reality</h1>
          </Col>
        </Row>
      </div>

      <br />
      <br />

      <div className="Content-SearchBar">
        <Row>
          <Col span="12" offset="6" align="center">
            <Search
              placeholder="input search text"
              enterButton="Search"
              size="large"
              prefix={selectBefore}
              onSearch={(value) => console.log(value)}
            />
          </Col>
        </Row>
      </div>

      <br />
      <br />

      <div className="Content-KnowledgeBase">
        <Row>
          <Col span="12" offset="6" align="center">
            <Dropdown overlay={menuKnowledgeBase}>
              <a
                className="ant-dropdown-link"
                onClick={(e) => e.preventDefault()}
              >
                Engineering Knowledge Base 
                <DownOutlined />
              </a>
            </Dropdown>
          </Col>
        </Row>
      </div>

      <br />
      <br />

      <div className="Content-Highlights">
        <Row>
          <Col span="6" offset="4" align="center">
            <Card style={{ width: 400 }}>
              <img src={Highlight1} alt="FabLanka Logo" width="60%"></img>;
              <h2>Highlight 01</h2>
              <p>Card content</p>
            </Card>
          </Col>
          <Col span="6" offset="0" align="center">
            <Card style={{ width: 400 }}>
              <img src={Highlight1} alt="FabLanka Logo" width="60%"></img>;
              <h2>Highlight 02</h2>
              <p>Card content</p>
            </Card>
          </Col>
          <Col span="6" offset="0" align="center">
            <Card style={{ width: 400 }}>
              <img src={Highlight1} alt="FabLanka Logo" width="60%"></img>;
              <h2>Highlight 03</h2>
              <p>Card content</p>
            </Card>
          </Col>
        </Row>
      </div>
    </React.Fragment>
  );
}

export default App;
